<!DOCTYPE html>

<html>
    <head>
        <title>Ejercicio1</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        En este ejercicio vamos a escribir textos en pantalla de diferentes modos
        
        <hr>
        <?php
        //Esto es un comentario
        echo "Texto escrito desde el script de PHP";
        /* esta es otra forma de comentar
         pero utilizando varias lineas */
        ?>
        <hr>
        Esto está escrito en HTML
        <hr>
        <?php
        #############################
        ####Esto es un comentario####
        #############################
        
        #En una pagina se pueden colocar varios scripts de php
        
        print("Esta es otra forma de escribir textos en la web mediante php");
        ?>
        
    </body>
</html>

